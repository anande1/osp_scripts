## [OSPD]

- Identify the new network CIDR,ip-pool-range,gateway,vlan/segmenetation-ID from the network-environment.yaml:

[stack@undercloud ~]$ grep -i external /home/stack/ams-templates-cps/network.yaml | grep -v '#'
  NeutronExternalNetworkBridge: "''"
  ExternalNetCidr: 10.81.68.0/24
  ExternalAllocationPools: [{'start': '10.81.68.62', 'end': '10.81.68.66'}]
  ExternalInterfaceDefaultRoute: 10.81.68.1
  ExternalNetworkVlanID: 3168

- Login to the DB to make changes:

$ mysql neutron

MariaDB [neutron]> select * from ipallocationpools;
+--------------------------------------+--------------------------------------+----------------+----------------+
| id                                   | subnet_id                            | first_ip       | last_ip        |
+--------------------------------------+--------------------------------------+----------------+----------------+
| 6f2b5f87-4d21-441b-a3d2-3cb59aefc843 | 87479bc7-2c83-4282-ab77-577f68d1aa1d | 11.118.0.101   | 11.118.0.200   |
| 70b2e331-edbf-4550-914c-33c5077c03a2 | 34f33519-7a44-4352-aafb-9142da4ffbf4 | 192.200.0.101  | 192.200.0.200  |
| 93cb898b-810f-42c1-9c58-1dcf3bcfbdd1 | 80446196-81ee-4a46-9d0d-ba610cf9fad6 | 172.21.202.111 | 172.21.202.150 |
| a3d66276-75b0-4496-ba67-33eb89e78cc9 | 48df3a56-f4fa-4174-bbf1-92764368bd55 | 11.120.0.101   | 11.120.0.200   |
| b2a03d50-9c77-4276-b573-26ff2c7e07ba | 399b5f82-24ba-4033-946f-d376d0b1e3ce | 11.117.0.101   | 11.117.0.200   |
| be8f120a-bb93-4c68-85b7-3b3614c71a07 | 14aba996-73c2-46eb-a542-68331d7cebda | 11.119.0.101   | 11.119.0.200   |
+--------------------------------------+--------------------------------------+----------------+----------------+
6 rows in set (0.00 sec)

MariaDB [neutron]> update ipallocationpools set first_ip = '10.81.68.66' where subnet_id = '80446196-81ee-4a46-9d0d-ba610cf9fad6';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [neutron]> update ipallocationpools set last_ip = '10.81.68.254' where subnet_id = '80446196-81ee-4a46-9d0d-ba610cf9fad6';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [neutron]> select * from subnets;
+----------------------------------+--------------------------------------+---------------------+--------------------------------------+------------+-----------------+--------------+-------------+--------------+-------------------+---------------+------------------+------------+
| project_id                       | id                                   | name                | network_id                           | ip_version | cidr            | gateway_ip   | enable_dhcp | ipv6_ra_mode | ipv6_address_mode | subnetpool_id | standard_attr_id | segment_id |
+----------------------------------+--------------------------------------+---------------------+--------------------------------------+------------+-----------------+--------------+-------------+--------------+-------------------+---------------+------------------+------------+
| 6c759b79cc12481cb41cfe29e7ac50ac | 14aba996-73c2-46eb-a542-68331d7cebda | storage_mgmt_subnet | af094e59-66ae-4fcd-89de-2df9f744604f |          4 | 11.119.0.0/24   | NULL         |           0 | NULL         | NULL              | NULL          |              860 | NULL       |
| 6c759b79cc12481cb41cfe29e7ac50ac | 34f33519-7a44-4352-aafb-9142da4ffbf4 |                     | 995b00bc-051b-497c-8b63-b364af175edb |          4 | 192.0.0.0/8     | 192.200.0.1  |           1 | NULL         | NULL              | NULL          |                8 | NULL       |
| 6c759b79cc12481cb41cfe29e7ac50ac | 399b5f82-24ba-4033-946f-d376d0b1e3ce | tenant_subnet       | a215d66a-095f-4969-bf27-050e34033394 |          4 | 11.117.0.0/24   | NULL         |           0 | NULL         | NULL              | NULL          |              858 | NULL       |
| 6c759b79cc12481cb41cfe29e7ac50ac | 48df3a56-f4fa-4174-bbf1-92764368bd55 | internal_api_subnet | c4874f41-0be1-4cff-b73c-3d4db3131477 |          4 | 11.120.0.0/24   | NULL         |           0 | NULL         | NULL              | NULL          |              850 | NULL       |
| 6c759b79cc12481cb41cfe29e7ac50ac | 80446196-81ee-4a46-9d0d-ba610cf9fad6 | external_subnet     | b1d065c3-4f63-41d0-a6ed-a188bf50cf96 |          4 | 172.21.202.0/24 | 172.21.202.1 |           0 | NULL         | NULL              | NULL          |              851 | NULL       |
| 6c759b79cc12481cb41cfe29e7ac50ac | 87479bc7-2c83-4282-ab77-577f68d1aa1d | storage_subnet      | 10569742-50a6-4bdd-b7f7-b7bb9e2c3b79 |          4 | 11.118.0.0/24   | NULL         |           0 | NULL         | NULL              | NULL          |              859 | NULL       |
+----------------------------------+--------------------------------------+---------------------+--------------------------------------+------------+-----------------+--------------+-------------+--------------+-------------------+---------------+------------------+------------+
6 rows in set (0.01 sec)

MariaDB [neutron]> update subnets set cidr='10.81.68.0/24' where name='external_subnet';
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [neutron]> update subnets set gateway_ip='10.81.68.1' where name='external_subnet';
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0


- Note the changes have been made:

[stack@undercloud ~]$ . stackrc
[stack@undercloud ~]$ neutron net-list
+--------------------------------------+--------------+----------------------------------------------------+
| id                                   | name         | subnets                                            |
+--------------------------------------+--------------+----------------------------------------------------+
| 10569742-50a6-4bdd-b7f7-b7bb9e2c3b79 | storage      | 87479bc7-2c83-4282-ab77-577f68d1aa1d 11.118.0.0/24 |
| 995b00bc-051b-497c-8b63-b364af175edb | ctlplane     | 34f33519-7a44-4352-aafb-9142da4ffbf4 192.0.0.0/8   |
| a215d66a-095f-4969-bf27-050e34033394 | tenant       | 399b5f82-24ba-4033-946f-d376d0b1e3ce 11.117.0.0/24 |
| af094e59-66ae-4fcd-89de-2df9f744604f | storage_mgmt | 14aba996-73c2-46eb-a542-68331d7cebda 11.119.0.0/24 |
| b1d065c3-4f63-41d0-a6ed-a188bf50cf96 | external     | 80446196-81ee-4a46-9d0d-ba610cf9fad6 10.81.68.0/24 |
| c4874f41-0be1-4cff-b73c-3d4db3131477 | internal_api | 48df3a56-f4fa-4174-bbf1-92764368bd55 11.120.0.0/24 |
+--------------------------------------+--------------+----------------------------------------------------+

- Now lets get the stale ipaddrsses on the old ports in use by the controllers and VIP :

[stack@undercloud ~]$ neutron port-list | grep 80446196-81ee-4a46-9d0d-ba610cf9fad6
| 2610185a-b68f-4c8b-a7c4-9f680cb2abe8 |                               | fa:16:3e:eb:74:5d | {"subnet_id": "80446196-81ee-4a46-9d0d-ba610cf9fad6", "ip_address": "172.21.202.118"} | >> new_ip = x/y/z
| 66352872-0322-401d-aa89-84930bb48987 |                               | fa:16:3e:6e:48:25 | {"subnet_id": "80446196-81ee-4a46-9d0d-ba610cf9fad6", "ip_address": "172.21.202.113"} | >> new_ip = x/y/z
| cf9982e8-96dd-40f3-8e49-f35b0c1669e0 |                               | fa:16:3e:6c:80:17 | {"subnet_id": "80446196-81ee-4a46-9d0d-ba610cf9fad6", "ip_address": "172.21.202.116"} | >> new_ip = x/y/z
| e216c2ea-95d2-45e7-afef-8b130c6af238 | public_virtual_ip             | fa:16:3e:5f:73:5a | {"subnet_id": "80446196-81ee-4a46-9d0d-ba610cf9fad6", "ip_address": "172.21.202.115"} | >> new_ip = 10.81.68.65

- To identify the "new ip" - login to every controller and check ifconfig of the vlan bridge of the external-network:
  (The format you need to look for in ifconfig us vlan<vlan_id> - example vlan101. The vlan_id can be obtained from 'ExternalNetworkVlanID' parameter)

[stack@undercloud ~]$ nova list | grep controller
| d4bf64c3-5862-4292-a29f-dd95e239114f | overcloud-controller-0  | ACTIVE | -          | Running     | ctlplane=192.200.0.109 | >> new ip : 10.81.68.62/24
| 96a2cfb1-6f63-4390-aaff-33094a9639e1 | overcloud-controller-1  | ACTIVE | -          | Running     | ctlplane=192.200.0.105 | >> new ip : 10.81.68.63/24
| a6a7c02e-88a1-4290-995c-6cbd7265c703 | overcloud-controller-2  | ACTIVE | -          | Running     | ctlplane=192.200.0.111 | >> new ip : 10.81.68.64/24

- To change the port-ip's safely stop the cluster.

[heat-admin@overcloudcontroller-0 ~]$ sudo pcs cluster stop --all
overcloud-controller-2: Stopping Cluster (pacemaker)...
overcloud-controller-1: Stopping Cluster (pacemaker)...
overcloud-controller-0: Stopping Cluster (pacemaker)...
overcloud-controller-2: Stopping Cluster (corosync)...
overcloud-controller-0: Stopping Cluster (corosync)...
overcloud-controller-1: Stopping Cluster (corosync)...

[stack@undercloud ~]$ openstack port unset --fixed-ip subnet=80446196-81ee-4a46-9d0d-ba610cf9fad6,ip-address=172.21.202.115 e216c2ea-95d2-45e7-afef-8b130c6af238
[stack@undercloud ~]$ openstack port unset --fixed-ip subnet=80446196-81ee-4a46-9d0d-ba610cf9fad6,ip-address=172.21.202.113 66352872-0322-401d-aa89-84930bb48987
[stack@undercloud ~]$ openstack port unset --fixed-ip subnet=80446196-81ee-4a46-9d0d-ba610cf9fad6,ip-address=172.21.202.116 cf9982e8-96dd-40f3-8e49-f35b0c1669e0
[stack@undercloud ~]$ openstack port unset --fixed-ip subnet=80446196-81ee-4a46-9d0d-ba610cf9fad6,ip-address=172.21.202.118 2610185a-b68f-4c8b-a7c4-9f680cb2abe8
[stack@undercloud ~]$ openstack port set --fixed-ip subnet=80446196-81ee-4a46-9d0d-ba610cf9fad6,ip-address=10.81.68.65 e216c2ea-95d2-45e7-afef-8b130c6af238
[stack@undercloud ~]$ openstack port set --fixed-ip subnet=80446196-81ee-4a46-9d0d-ba610cf9fad6,ip-address=10.81.68.6x 66352872-0322-401d-aa89-84930bb48987
[stack@undercloud ~]$ openstack port set --fixed-ip subnet=80446196-81ee-4a46-9d0d-ba610cf9fad6,ip-address=10.81.68.6y cf9982e8-96dd-40f3-8e49-f35b0c1669e0
[stack@undercloud ~]$ openstack port set --fixed-ip subnet=80446196-81ee-4a46-9d0d-ba610cf9fad6,ip-address=10.81.68.6z 2610185a-b68f-4c8b-a7c4-9f680cb2abe8

[stack@undercloud ~]$ neutron port-list | grep 80446196-81ee-4a46-9d0d-ba610cf9fad6 | grep e216c2ea-95d2-45e7-afef-8b130c6af238
| e216c2ea-95d2-45e7-afef-8b130c6af238 | public_virtual_ip             | fa:16:3e:5f:73:5a | {"subnet_id": "80446196-81ee-4a46-9d0d-ba610cf9fad6", "ip_address": "10.81.68.65"}

## The x/y/z needs to be identified based on previous ip-assignments. One can determine that using 'ifconfig' output from all controllers *taken previously*.


- Start the cluster back:

[heat-admin@overcloudcontroller-0 ~]$ sudo pcs cluster start --all
- Confirm if you can access the dashboard and use various cli commands.
- Try creating new instances with floating_ip which will from the new vlan_range
